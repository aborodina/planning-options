import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {finalize} from 'rxjs/operators';

import {GroupWithParameterModel} from '../shared/models/group-with-parameter.model';
import {GroupWithParameterService} from '../shared/services/group-with-parameter.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListComponent implements OnInit {
  public groupsWithParamList: GroupWithParameterModel[];
  public loadIndicator = false;
  public loadingData = false;
  public simplifiedMode = false;

  constructor(private parameterListService: GroupWithParameterService,
              private cdr: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.getGroupWithParam();
  }

  getGroupWithParam() {
    this.loadingData = true;
    this.parameterListService
      .getGroupListWithParam()
      .pipe(
        finalize(() => {
          this.loadingData = false;
          this.cdr.markForCheck();
        })
      )
      .subscribe(value => this.groupsWithParamList = value);
  }

  calculate() {
    this.loadIndicator = true;

    this.parameterListService
      .setGroupListWithParam(this.groupsWithParamList)
      .pipe(
        finalize(() => {
          this.loadIndicator = false;
          this.cdr.markForCheck();
        })
      )
      .subscribe();
  }
}
