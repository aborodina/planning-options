import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListComponent } from './list.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {SimplifiedModeFilterPipe} from '../shared/pipes/simplified-mode-filter.pipe';
import {GroupWithParameterService} from '../shared/services/group-with-parameter.service';
import {GroupService} from '../shared/services/group.service';
import {ParameterService} from '../shared/services/parameter.service';

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListComponent, SimplifiedModeFilterPipe ],
      schemas: [
        NO_ERRORS_SCHEMA
      ],
      providers: [GroupService, ParameterService, GroupWithParameterService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
