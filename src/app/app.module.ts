import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {
  DevExtremeModule, DxButtonModule,
  DxLoadIndicatorModule,
  DxNumberBoxModule,
  DxRangeSliderModule,
  DxTextBoxModule,
} from 'devextreme-angular';
import {MatExpansionModule, MatProgressBarModule} from '@angular/material';

import {AppComponent} from './app.component';
import {GroupService} from './shared/services/group.service';
import {GroupWithParameterService} from './shared/services/group-with-parameter.service';
import {ListComponent} from './list/list.component';
import {ParameterComponent} from './parameter/parameter.component';
import {ParameterService} from './shared/services/parameter.service';
import {SimplifiedModeFilterPipe} from './shared/pipes/simplified-mode-filter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    ParameterComponent,
    SimplifiedModeFilterPipe
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    DevExtremeModule,
    DxButtonModule,
    DxLoadIndicatorModule,
    DxNumberBoxModule,
    DxRangeSliderModule,
    DxTextBoxModule,
    MatExpansionModule,
    MatProgressBarModule
  ],
  providers: [GroupService, ParameterService, GroupWithParameterService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
