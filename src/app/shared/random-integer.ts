// случайное число от min до max
export function randomInteger(min: number, max: number): number {
  return Math.floor(min + Math.random() * (max + 1 - min));
}
