import {Pipe, PipeTransform} from '@angular/core';

import {randomInteger} from '../random-integer';
import {ParameterModel} from '../models/parameter.model';

@Pipe({
  name: 'simplifiedModeFilter'
})
export class SimplifiedModeFilterPipe implements PipeTransform {

  transform(items: ParameterModel[], simplifiedMode: boolean): ParameterModel[] {
    if (simplifiedMode) {
      const random = randomInteger(0, items.length - 1);
      return items.filter((value, index) => index !== random);
    } else {
      return items;
    }
  }
}
