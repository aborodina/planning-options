import {SimplifiedModeFilterPipe} from './simplified-mode-filter.pipe';
import {ParameterModel} from '../models/parameter.model';

describe('SimplifiedModeFilterPipe', () => {
  let pipe: SimplifiedModeFilterPipe;

  beforeEach(() => {
    pipe = new SimplifiedModeFilterPipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  describe('#transform', () => {
    it('return all items', () => {
      const items = [{name: 'p1'}, {name: 'p2'}] as ParameterModel[];
      expect(pipe.transform(items, false)).toEqual(items);
    });

    it('return items -1', () => {
      const items = [{name: 'p1'}, {name: 'p2'}] as ParameterModel[];
      expect(pipe.transform(items, true).length).toBe(items.length - 1);
    });
  });
});
