export class ParameterModel {
  id: number;
  groupId: number;
  name: string;
  min: number;
  max: number;
}
