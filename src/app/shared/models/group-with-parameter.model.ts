import {ParameterModel} from './parameter.model';
import {GroupModel} from './group.model';

export class GroupWithParameterModel extends GroupModel {
  items: ParameterModel[];
}
