import {GroupModel} from '../models/group.model';

export const groupsMock: GroupModel[] = [
  {
    id: 1,
    name: 'Группа параметров 1',
  },
  {
    id: 2,
    name: 'Группа параметров 2'
  },
  {
    id: 3,
    name: 'Группа параметров 3'
  },
  {
    id: 4,
    name: 'Группа параметров 4'
  },
  {
    id: 5,
    name: 'Группа параметров 5'
  },
];
