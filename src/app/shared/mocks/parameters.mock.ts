import {ParameterModel} from '../models/parameter.model';

export const parametersMock: ParameterModel[] = [
  {
    id: 1,
    groupId: 1,
    name: 'Параметр 1',
    min: 10,
    max: 80
  },
  {
    id: 2,
    groupId: 1,
    name: 'Параметр 2',
    min: 20,
    max: 70
  },
  {
    id: 3,
    groupId: 1,
    name: 'Параметр 3',
    min: 20,
    max: 90
  },
  {
    id: 4,
    groupId: 2,
    name: 'Параметр 1',
    min: 10,
    max: 80
  },
  {
    id: 5,
    groupId: 2,
    name: 'Параметр 2',
    min: 30,
    max: 70
  },
  {
    id: 6,
    groupId: 2,
    name: 'Параметр 3',
    min: 10,
    max: 70
  },
  {
    id: 7,
    groupId: 3,
    name: 'Параметр 1',
    min: 10,
    max: 80
  },
  {
    id: 8,
    groupId: 3,
    name: 'Параметр 2',
    min: 30,
    max: 70
  },
  {
    id: 9,
    groupId: 3,
    name: 'Параметр 3',
    min: 10,
    max: 70
  },
  {
    id: 10,
    groupId: 4,
    name: 'Параметр 1',
    min: 10,
    max: 80
  },
  {
    id: 11,
    groupId: 4,
    name: 'Параметр 2',
    min: 30,
    max: 70
  },
  {
    id: 12,
    groupId: 4,
    name: 'Параметр 3',
    min: 10,
    max: 70
  },
  {
    id: 13,
    groupId: 5,
    name: 'Параметр 1',
    min: 10,
    max: 80
  },
  {
    id: 14,
    groupId: 5,
    name: 'Параметр 2',
    min: 30,
    max: 70
  },
  {
    id: 15,
    groupId: 5,
    name: 'Параметр 3',
    min: 10,
    max: 70
  },
];
