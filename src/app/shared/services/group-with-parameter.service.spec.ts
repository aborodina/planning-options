import {TestBed} from '@angular/core/testing';
import {GroupWithParameterService} from './group-with-parameter.service';
import {GroupService} from './group.service';
import {ParameterService} from './parameter.service';
import {GroupModel} from '../models/group.model';
import {ParameterModel} from '../models/parameter.model';
import {GroupWithParameterModel} from '../models/group-with-parameter.model';

describe('GroupWithParameterService', () => {
  let service: GroupWithParameterService;

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      GroupWithParameterService,
      GroupService,
      ParameterService
    ]
  }));

  beforeEach(() => {
    service = TestBed.get(GroupWithParameterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#concatGroupAndParam', () => {
    it('should be concat group with param', () => {
      expect(service.concatGroupAndParam(group, param)).toEqual(groupWithParam);
    });
  });

  describe('#divideGroupAndParam', () => {
    it('should be divide group with param', () => {
      expect(service.divideGroupAndParam(groupWithParam)).toEqual([group, param]);
    });
  });
});

const group: GroupModel[] = [
  {id: 1, name: 'g1'},
  {id: 2, name: 'g2'}
];
const param: ParameterModel[] = [
  {id: 1, name: 'p1', groupId: 1, min: 10, max: 90},
  {id: 2, name: 'p2', groupId: 1, min: 10, max: 90},
  {id: 4, name: 'p1', groupId: 2, min: 10, max: 90},
];
const groupWithParam: GroupWithParameterModel[] = [
  {
    id: 1,
    name: 'g1',
    items: [
      {id: 1, name: 'p1', groupId: 1, min: 10, max: 90},
      {id: 2, name: 'p2', groupId: 1, min: 10, max: 90}
    ]
  },
  {
    id: 2,
    name: 'g2',
    items: [
      {id: 4, name: 'p1', groupId: 2, min: 10, max: 90},
    ]
  }
];
