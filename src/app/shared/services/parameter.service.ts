import {Injectable} from '@angular/core';
import {delay} from 'rxjs/operators';
import {Observable, of} from 'rxjs';

import {ParameterModel} from '../models/parameter.model';
import {parametersMock} from '../mocks/parameters.mock';

@Injectable()
export class ParameterService {

  getParameter(): Observable<ParameterModel[]> {
    return of(parametersMock).pipe(
      delay(1000)
    );
  }

  setParameter(parameterList): Observable<boolean> {
    console.log('Parameter save', parameterList);
    return of(true).pipe(
      delay(1000)
    );
  }
}
