import {Injectable} from '@angular/core';
import {combineLatest, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {GroupModel} from '../models/group.model';
import {GroupService} from './group.service';
import {GroupWithParameterModel} from '../models/group-with-parameter.model';
import {ParameterModel} from '../models/parameter.model';
import {ParameterService} from './parameter.service';

@Injectable()
export class GroupWithParameterService {
  constructor(public groupService: GroupService,
              public parameterService: ParameterService) {
  }

  getGroupListWithParam(): Observable<GroupWithParameterModel[]> {
    return combineLatest(this.groupService.getGroups(), this.parameterService.getParameter())
      .pipe(
        map(([groups, params]: [GroupModel[], ParameterModel[]]) => {
          return this.concatGroupAndParam(groups, params);
        })
      );
  }

  concatGroupAndParam(groups: GroupModel[], params: ParameterModel[]): GroupWithParameterModel[] {
    return groups.map((group) => ({
      name: group.name,
      id: group.id,
      items: params.filter((item) => item.groupId === group.id)
    }));
  }

  setGroupListWithParam(groupsWithParamList: GroupWithParameterModel[]): Observable<boolean> {
    const [groupList, parameterList]: [GroupModel[], ParameterModel[]] = this.divideGroupAndParam(groupsWithParamList);
    return combineLatest(this.groupService.setGroup(groupList), this.parameterService.setParameter(parameterList)).pipe(
      map(() => {
        return true;
      })
    );
  }

  divideGroupAndParam(groupsWithParamList: GroupWithParameterModel[]): [GroupModel[], ParameterModel[]] {
    const groupList: GroupModel[] = groupsWithParamList.map((group => ({name: group.name, id: group.id})));

    const parameterList: ParameterModel[] = groupsWithParamList.reduce((acc, current) => acc.concat(current.items), []);

    return [groupList, parameterList];
  }
}
