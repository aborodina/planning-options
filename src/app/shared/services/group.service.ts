import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {delay} from 'rxjs/operators';

import {GroupModel} from '../models/group.model';
import {groupsMock} from '../mocks/groups.mock';

@Injectable()
export class GroupService {
  getGroups(): Observable<GroupModel[]> {
    return of(groupsMock).pipe(
      delay(100)
    );
  }

  setGroup(groupList): Observable<boolean> {
    console.log('Group save', groupList);
    return of(true).pipe(
      delay(1000)
    );
  }
}




