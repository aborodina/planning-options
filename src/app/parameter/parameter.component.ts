import {Component, ChangeDetectionStrategy, Input} from '@angular/core';

import {ParameterModel} from '../shared/models/parameter.model';

@Component({
  selector: 'app-parameter',
  templateUrl: './parameter.component.html',
  styleUrls: ['./parameter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ParameterComponent {

  @Input() parameter: ParameterModel;
  @Input() disabled = false;
}
